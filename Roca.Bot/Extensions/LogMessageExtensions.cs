﻿using Discord;
using Microsoft.Extensions.Logging;

namespace Roca.Bot.Extensions;

internal static class LogMessageExtensions
{
    public static void LogTo(this LogMessage arg, ILogger logger)
    {
        LogLevel level = arg.Severity switch
        {
            LogSeverity.Warning => LogLevel.Warning,
            LogSeverity.Verbose => LogLevel.Trace,
            LogSeverity.Info => LogLevel.Information,
            LogSeverity.Error => LogLevel.Error,
            LogSeverity.Debug => LogLevel.Debug,
            LogSeverity.Critical => LogLevel.Critical,
            _ => LogLevel.None
        };
        logger.Log(level, arg.Exception, "{source}: {msg}", arg.Source, arg.Message);
    }
}
