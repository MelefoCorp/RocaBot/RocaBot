﻿using Discord;
using Discord.Interactions;
using Discord.WebSocket;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json.Linq;
using Roca.Bot.Extensions;
using Roca.Bot.Handlers;
using Roca.Bot.Services;

namespace Roca.Bot;

public class RocaBot
{
    private readonly DiscordShardedClient _client;
    private readonly IConfiguration _config;
    private readonly ILogger _logger;
    private readonly IServiceProvider _services;
    private CancellationToken _token;
    private int ShardsReady;

    public RocaBot(IConfiguration config, ILogger logger)
    {
        _config = config;
        _logger = logger;
        _client = new(new DiscordSocketConfig()
        {
            LogLevel = LogSeverity.Info,
            DefaultRetryMode = RetryMode.AlwaysRetry,
            AlwaysDownloadDefaultStickers = true,
            AlwaysDownloadUsers = true,
            AlwaysResolveStickers = true,
            GatewayIntents = GatewayIntents.All,
            MessageCacheSize = 1000,
        });

        _client.Log += OnLog;
        _client.ShardReady += OnShardReady;

        _services = ConfigureServices();
    }

    private async Task OnShardReady(DiscordSocketClient _)
    {
        if (++ShardsReady != _client.Shards.Count)
            return;
        var services = _services.GetServices<IHostedService>();
        foreach (var service in services)
            await service.StartAsync(_token);
    }

    private Task OnLog(LogMessage msg)
    {
        msg.LogTo(_logger);
        return Task.CompletedTask;
    }

    private IServiceProvider ConfigureServices()
    {
        MongoService.Initialize(_config);
        return new ServiceCollection()
            .AddSingleton(this)
            .AddSingleton(_config)
            .AddSingleton(_logger)
            .AddSingleton(_client)
            .AddSingleton<InteractionService>()
            .AddHostedService<CommandHandler>()
            .BuildServiceProvider();
    }

    public async Task StartAsync(CancellationToken token)
    {
        _token = token;
        await _client.LoginAsync(TokenType.Bot, _config["Roca:Bot:Token"]);
        await _client.StartAsync();
    }
}