﻿using Discord;
using Discord.Interactions;
using Humanizer;
using Microsoft.VisualBasic;
using System.Diagnostics;
using System.Globalization;

namespace Roca.Bot.Modules.Self;

public class Commands : RocaBase
{
    private readonly Emoji _pong = new("🏓");
    private readonly List<string> _catchphrase = new()
    {
        "Damn it! You beat me",
        "It was fun! Let’s play again!",
        "Wow, you're good at this!",
        "You're up!"
    };
    private readonly Color _color = new(114, 137, 218);

    private static IEmote IsPongOk(double nbr, double max) =>
        (nbr / max) switch
        {
            <= (1.0 / 3.0) => Emote.Parse("<:cross:634736883330580490>"),
            >= (2.0 / 3.0) => new Emoji("✅"),
            _ => Emote.Parse("<:cross:634736883330580490>")
        };


    [SlashCommand("ping", "Pong!")]
    public async Task Ping()
    {
        Stopwatch edit = new();
        Stopwatch react = new();

        await RespondAsync("🏓 Waiting for the ball");

        edit.Start();
        var msg = await ModifyOriginalResponseAsync(x => x.Content = "🏓 Waiting for the ball...");
        edit.Stop();

        react.Start();
        await msg.AddReactionAsync(_pong);
        react.Stop();
        await msg.RemoveAllReactionsAsync();

        var shards = Context.Client.Shards.Count(x => x.ConnectionState == ConnectionState.Connected);
        var guilds = Context.Client.Guilds.Count(x => x.IsConnected && x.IsSynced);

        await ModifyOriginalResponseAsync(x =>
        {
            x.Content = "";
            x.Embed = new EmbedBuilder
            {
                Author = new EmbedAuthorBuilder
                {
                    IconUrl = "https://emojipedia-us.s3.amazonaws.com/source/microsoft-teams/337/ping-pong_1f3d3.png",
                    Name = "Is RocaBot Down?"
                },
                Description = $"{_catchphrase[Random.Shared.Next(0, _catchphrase.Count)]}\n\nModification time: `{edit.ElapsedMilliseconds}ms`\nReact time: `{react.ElapsedMilliseconds}ms`\nResponse time: `{Context.Client.Latency}ms`\n\n{IsPongOk(shards, Context.Client.Shards.Count)} {shards}/{Context.Client.Shards.Count} shards\n{IsPongOk(guilds, Context.Client.Guilds.Count)} {guilds}/{Context.Client.Guilds.Count} servers",
                Color = _color,
                Footer = new EmbedFooterBuilder
                {
                    Text = Context.Client.Latency switch
                    {
                        >= 200 and < 500 => "Pretty fast, if you ask me.",
                        >= 500 and < 1200 => "The connection is OK.",
                        >= 1200 and < 5000 => "Um... it seems a little slow...",
                        >= 5000 and < 10000 => "Zzzzzz.... Huh? What? It’s good it’s here?",
                        > 10000 => "I’ve been waiting forever for the ball to come back!",
                        _ => "Holy dog, it’s REALLY fast"
                    }
                }

            }.Build();
        });
    }

    [RequireUserPermission(GuildPermission.Administrator)]
    [SlashCommand("say", "Echo a message to a channel")]
    public async Task Say(ITextChannel channel, string message)
    {
        await RespondAsync("Message sent.", ephemeral: true);
        await channel.SendMessageAsync(message);
    }

    [SlashCommand("dm", "Send a direct message to the owner")]
    public async Task DM(string message)
    {
        await RespondAsync("Message sent.", ephemeral: true);

        var app = await Context.Client.GetApplicationInfoAsync().ConfigureAwait(false);
        await app.Owner.SendMessageAsync(embed: new EmbedBuilder
        {
            Author = new()
            {
                Name = $"{Context.User} ({Context.User.Id})"
            },
            Description = message,
            Timestamp = DateTime.UtcNow,
            Color = _color
        }.Build());
    }

    private static TimeSpan GetUptime() => DateTime.UtcNow - Process.GetCurrentProcess().StartTime.ToUniversalTime();
    private static string GetHeapSize() => Math.Round(GC.GetTotalMemory(true) / (1024.0 * 1024.0), 2).ToString(CultureInfo.CurrentCulture);


    [SlashCommand("version", "Get current RocaBot version information")]
    public async Task Version()
    {
        var app = await Context.Client.GetApplicationInfoAsync().ConfigureAwait(false);

        await RespondAsync(embed: new EmbedBuilder
        {
            Author = new()
            {
                Name = Context.Client.CurrentUser.ToString(),
                IconUrl = Context.Client.CurrentUser.GetAvatarUrl() ?? Context.Client.CurrentUser.GetDefaultAvatarUrl()
            },
            Color = _color,
            ThumbnailUrl = app.Owner.GetAvatarUrl() ?? app.Owner.GetDefaultAvatarUrl(),
            Description = $"I'm a **BOT** written in __C#__ created by {app.Owner.Mention} originally for [Newtiteuf](https://youtube.com/newtiteufprod) and is Discord server [NT Family](https://discord.gg/newtiteuf)\n\n[Support server](https://discord.gg/RASajC8) | [Website](https://rocabot.xyz)\n\nCurrent version: `v0.15.0-rewrite-hardcoded`\nDiscord.NET: `{DiscordConfig.Version}`\nUptime: `{GetUptime().Humanize()}`\nMemory used: `{GetHeapSize()}MB`\nServers/Channels/Members: `{Context.Client.Guilds.Count}/{Context.Client.Guilds.Sum(x => x.Channels.Count)}/{Context.Client.Guilds.Sum(x => x.MemberCount)}`"
        }.Build());
    }
}