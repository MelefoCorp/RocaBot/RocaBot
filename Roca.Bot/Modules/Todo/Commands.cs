﻿using Discord.Interactions;
using Roca.Bot.Services;

namespace Roca.Bot.Modules.Todo;

[Group("todo", "Todo module")]
public class Commands : RocaBase
{
    [SlashCommand("view", "View your current todo list")]
    public async Task View()
    {
        if (Context.UserAccount.ToDo.Count == 0)
        {
            await RespondAsync("Your todo list is empty.");
            return;
        }
        string str = "";
        for (int i = 0; i < Context.UserAccount.ToDo.Count; i++)
            str += $"{i + 1}. {Context.UserAccount.ToDo[i]}\n";
        await RespondAsync($"Your todo list: ```{str}```");
    }

    [SlashCommand("add", "Add an element to your todo list")]
    public async Task Add(string content)
    {
        Context.UserAccount.ToDo.Add(content);
        await Context.UserAccount.Save();

        string str = "";
        for (int i = 0; i < Context.UserAccount.ToDo.Count; i++)
            str += $"{i + 1}. {Context.UserAccount.ToDo[i]}\n";
        await RespondAsync($"Your *modified* todo list: ```{str}```");
    }

    [SlashCommand("delete", "Delete an element of your todo list")]
    public async Task Delete(int index)
    {
        Context.UserAccount.ToDo.RemoveAt(index);
        await Context.UserAccount.Save();

        string str = "";
        for (int i = 0; i < Context.UserAccount.ToDo.Count; i++)
            str += $"{i + 1}. {Context.UserAccount.ToDo[i]}\n";
        await RespondAsync($"Your *modified* todo list: ```{str}```");
    }

    [SlashCommand("clear", "Delete your whole todo list")]
    public async Task Clear()
    {
        Context.UserAccount.ToDo.Clear();
        await Context.UserAccount.Save();

        await RespondAsync("Your todo list is now empty.");
    }

    [SlashCommand("edit", "Edit an element of your todo list")]
    public async Task Edit(int index, string content)
    {
        if (index < 1 || index > Context.UserAccount.ToDo.Count)
        {
            await RespondAsync($"your index must be between 1 and {Context.UserAccount.ToDo.Count}");
            return;
        }
        Context.UserAccount.ToDo[index - 1] = content;
        await Context.UserAccount.Save();

        string str = "";
        for (int i = 0; i < Context.UserAccount.ToDo.Count; i++)
            str += $"{i + 1}. {Context.UserAccount.ToDo[i]}\n";
        await RespondAsync($"Your *modified* todo list: ```{str}```");
    }
}
