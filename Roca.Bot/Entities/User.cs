﻿using MongoDB.Bson;

namespace Roca.Bot.Entities;

public class User : IEntity
{
    public ObjectId ObjectId { get; set; } = ObjectId.GenerateNewId();
    public ulong Id { get; set; }

    public List<string> ToDo { get; set; } = new();
}
