﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace Roca.Bot.Entities;

public interface IEntity
{

    [BsonId]
    public ObjectId ObjectId { get; set; }
    public ulong Id { get; set; }
}
