﻿using Discord;
using Microsoft.Extensions.Configuration;
using MongoDB.Driver;
using Roca.Bot.Entities;
using System.Collections.Concurrent;
using System.Linq.Expressions;

namespace Roca.Bot.Services;

public static class MongoService
{
    private static MongoClient? _client;
    private static IMongoDatabase? _database;

    public static void Initialize(IConfiguration config)
    {
        _client = new(config["Mongo:ConnectionString"]);
        _database = _client.GetDatabase("RocaBot-dev");
    }

    public static async Task Save<T>(this T entity) where T : IEntity =>
        await _database!.GetCollection<T>(typeof(T).Name).ReplaceOneAsync(x => x.Id == entity.Id, entity, new ReplaceOptions
        {
            IsUpsert = true
        });

    public static async Task Delete<T>(this T entity) where T : IEntity =>
        await _database!.GetCollection<T>(typeof(T).Name).DeleteOneAsync(x => x.Id == entity.Id);

    public static async Task<T> Find<T>(ulong id) where T : IEntity, new()
    {
        var result = await _database!.GetCollection<T>(typeof(T).Name).FindAsync(x => x.Id == id);
        return await result.SingleOrDefaultAsync() ?? new T() { Id = id };
    }

    public static async Task<List<T>> FindAll<T>() where T : IEntity
    {
        var result = await _database!.GetCollection<T>(typeof(T).Name).FindAsync(Builders<T>.Filter.Empty);
        return await result.ToListAsync();
    }

    public static async Task<List<T>> Findfiltered<T>(Expression<Func<T, bool>> filter) where T : IEntity
    {
        var result = await _database!.GetCollection<T>(typeof(T).Name).FindAsync(filter);
        return await result.ToListAsync();
    }

    public static User GetAccount(this IUser user)
    {
        var result = _database!.GetCollection<User>("User").Find(x => x.Id == user.Id);
        return result.SingleOrDefault() ?? new User() { Id = user.Id };
    }

}