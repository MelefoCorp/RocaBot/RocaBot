﻿using Discord.Interactions;
using Discord.WebSocket;
using Roca.Bot.Entities;
using Roca.Bot.Services;

namespace Roca.Bot;

public class RocaContext : ShardedInteractionContext
{
    public readonly User UserAccount;
    public RocaContext(DiscordShardedClient client, SocketInteraction interaction) : base(client, interaction)
    {
        UserAccount = interaction.User.GetAccount();
    }
}
