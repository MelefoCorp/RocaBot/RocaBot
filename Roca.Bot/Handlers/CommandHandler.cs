﻿using Discord.Interactions;
using Discord.WebSocket;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using System.Reflection;
using Roca.Bot.Extensions;
using Discord;

namespace Roca.Bot.Handlers;

public class CommandHandler : BackgroundService
{
    private readonly IServiceProvider _services;
    private readonly ILogger _logger;
    private readonly DiscordShardedClient _client;
    private readonly InteractionService _interaction;

    public CommandHandler(IServiceProvider services)
    {
        _services = services;
        _logger = services.GetRequiredService<ILogger>();
        _client = _services.GetRequiredService<DiscordShardedClient>();
        _interaction = services.GetRequiredService<InteractionService>();

        _client.InteractionCreated += OnInteractionCreated;
        _interaction.Log += OnLog;
    }

    private Task OnLog(LogMessage msg)
    {
        msg.LogTo(_logger);
        return Task.CompletedTask;
    }

    private Task OnInteractionCreated(SocketInteraction interaction)
    {
        _ = Task.Run(async () =>
        {
            var context = new RocaContext(_client, interaction);
            await _interaction.ExecuteCommandAsync(context, _services);
        });
        return Task.CompletedTask;
    }

    protected override async Task ExecuteAsync(CancellationToken _)
    {
        await _interaction.AddModulesAsync(Assembly.GetExecutingAssembly(), _services);
        await _interaction.RegisterCommandsGloballyAsync();
    }
}
