﻿using Discord.Interactions;

namespace Roca.Bot;

public class RocaBase : RocaBase<RocaContext>
{

}

public class RocaBase<T> : InteractionModuleBase<T>
    where T : RocaContext
{
}
