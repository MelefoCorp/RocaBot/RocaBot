#See https://aka.ms/containerfastmode to understand how Visual Studio uses this Dockerfile to build your images for faster debugging.

FROM mcr.microsoft.com/dotnet/aspnet:7.0 AS base
WORKDIR /app
EXPOSE 80
EXPOSE 443

FROM mcr.microsoft.com/dotnet/sdk:7.0 AS build
WORKDIR /src
COPY ["Roca.Api/Roca.Api.csproj", "Roca.Api/"]
RUN dotnet restore "Roca.Api/Roca.Api.csproj"
COPY . .
WORKDIR "/src/Roca.Api"
RUN dotnet build "Roca.Api.csproj" -c Release -o /app/build

FROM build AS publish
RUN dotnet publish "Roca.Api.csproj" -c Release -o /app/publish /p:UseAppHost=false

FROM base AS final
WORKDIR /app
COPY --from=publish /app/publish .
ENTRYPOINT ["dotnet", "Roca.Api.dll"]