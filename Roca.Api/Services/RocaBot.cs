﻿namespace Roca.Api.Services
{
    public class RocaBot : BackgroundService
    {
        public readonly Bot.RocaBot Client;

        public RocaBot(IConfiguration config, ILogger<RocaBot> logger) =>
            Client = new(config, logger);

        protected override async Task ExecuteAsync(CancellationToken stoppingToken) =>
            await Client.StartAsync(stoppingToken);
    }
}
